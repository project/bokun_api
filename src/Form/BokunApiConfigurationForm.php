<?php
namespace Drupal\bokun_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form definition for API keys and URL
 *
 * @author Hilmar Kári Hallbjörnsson (drupalviking) - hilmar@umadgera.is
 * @author Um að gera ehf.
 */
class BokunApiConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bokun_api.configuration'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bokun_api_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bokun_api.configuration');

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bókun API key'),
      '#description' => $this->t('Please provide the public API key for Bókun'),
      '#default_value' => $config->get('api_key'),
    ];

    $form['api_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bókun API secret'),
      '#description' => $this->t('Please provide the secret API key for Bókun (only used for hashing, will never be broadcasted.'),
      '#default_value' => $config->get('api_secret'),
    ];

    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The URL to the API'),
      '#description' => $this->t('Please provide the URL to the API.'),
      '#default_value' => $config->get('api_url'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('bokun_api.configuration')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_secret', $form_state->getValue('api_secret'))
      ->set('api_url', $form_state->getValue('api_url'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
