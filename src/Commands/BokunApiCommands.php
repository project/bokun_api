<?php
namespace Drupal\bokun_api\Commands;

use Drupal\bokun_api\BokunApiService;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 * - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 * - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */

class BokunApiCommands extends DrushCommands {
  /**
   * Tests the API calls
   *
   * @command bokunapi:test_api
   * @usage bokunapi:test_api
   *   Displays response from the Bókun API
   */
  public function testApi() {
    /** @var \Drupal\bokun_api\BokunApiService  $service */
    $service = \Drupal::service('bokun_api.booking_service');
    #$data = $service->getAllProductLists();
  }
}
