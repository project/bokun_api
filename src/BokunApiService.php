<?php

namespace Drupal\bokun_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\Plugin\views\argument\NullArgument;

class BokunApiService {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * BokunApiService constructor.
   *
   * @param ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   *********************** OPERATIONS FOR TOURS & ACTIVITIES *******************
   */

  /**
   * Fetches one Activity item from the Bókun API.
   * Use unique ID for fetching comprehensive information on this activity product.
   *
   * @param string $id
   *   The ID of the activity
   * @param string $currency
   *   The requested Currency value. The value should be uppercase,
   *   but the function will uppercase the string, just "in case".
   * @param $language
   *   The language of the data, we would like to fetch.
   * @return string
   *   JSON string with data
   */
  public function getActivityItem($id, $currency = "EUR", $language = "EN") {
    $currency = strtoupper($currency);
    $path = "/activity.json/{$id}?currency={$currency}&lang={$language}";
    return $this->apiQuery( $path );
  }

  /**
   * @param $id
   *   The ID of the activity
   * @param string $start_date
   *   The start date of the fetched data, in the format YYYY-mm-dd
   * @param null $end_date
   *   The end date of the fetched data, in the format YYYY-mm-dd
   * @return string
   *   JSON string with data
   */
  public function getActivityItemAvailabilities($id, $start_date = NULL, $end_date = NULL) {
    if(!isset($start_date)) {
      $start_date = date('Y-m-d');
    }
    if(!isset($enddate)) {
      $end_date = date('Y');
      $end_date .= $end_date . '-12-31';
    }
    $path = "/activity.json/{$id}/availabilities?start={$start_date}&end={$end_date}";

    return $this->apiQuery($path);
  }

  /**
   * Get the list of upcoming availabilities for the activity id
   * (including sold-outs, if parameter "includeSoldOut" == true.)
   *
   * @param $id
   *   The ID of the activity
   * @param int $max_hits
   *   The max number of upcoming availabilities to get.
   * @param string $lang
   *   The language the content should be served in.
   * @param false $include_sold_out
   *   Specify whether to include availabilities that are sold out.
   * @return string
   *   JSON string with data
   */
  public function getUpcomingAvailabilities($id, $max_hits = 10, $lang = 'EN',
                                            $include_sold_out = FALSE) {
    $path = "/activity.json/{$id}/upcoming-availabilities/{$max_hits}?
    lang={$lang}&includeSoldOut={$include_sold_out}";

    return $this->apiQuery($path);
  }

  /**
   * Use corresponding slug (human readable string that is used to identify
   * the product) for fetching this product.
   * Slug is set in Product overview -> Actions -> Slugs.
   *
   * @param string $slug
   *   The slug of the activity to be fetched
   * @param string $lang
   *   The language the content should be served in
   * @return string
   *   JSON string with data
   */
  public function getActivityBySlug($slug, $lang = 'EN') {
    $path = "/activity.json/slug/{$slug}?lang={$lang}";

    return $this->apiQuery($path);
  }

  /**
   * Gets all pickup and dropoff places for an activity
   *
   * @param $activity_id
   *   The ID of the activity
   * @param string $currency
   *   The requested Currency value. The value should be uppercase,
   *   but the function will uppercase the string, just "in case".
   * @param string $lang
   *   The language of the data, we would like to fetch.
   * @return string
   *   JSON string with data
   */
  public function getPickupPlacesForActivities($activity_id, $currency = 'EUR',
                                               $lang = 'EN') {
    $currency = strtoupper($currency);
    $path = "/activity.json/{$activity_id}/pickup-places?currency={$currency}&lang={$lang}";

    return $this->apiQuery($path);
  }

  /**
   * Gets IDs of all active activities that can be booked.
   *
   * @return string
   *   JSON string with data
   */
  public function getActiveIdsForActivities() {
    $path = "/activity.json/active-ids";

    return $this->apiQuery($path);
  }

  /**
   * Get ids/timestamps of all Activities modified in specified date range
   *
   * @param $fromDate
   *   Date range start inclusive
   * @param $toDate
   *   Date range end exclusive
   * @return string
   *   JSON string with data
   */
  public function getUpdatedListForActivities($fromDate, $toDate = NULL) {
    if($toDate) {
      $path = "/activity.json/list-updated?fromDate={$fromDate}&toDate={$toDate}";
    }
    else {
      $path = "/activity.json/list-updated?fromDate={$fromDate}";
    }

    return $this->apiQuery($path);
  }

  /**
   * Get activity price-list
   *
   * @param $activity_id
   *   The ID of the activity
   * @param string $currency
   *   The requested Currency value. The value should be uppercase,
   *   but the function will uppercase the string, just "in case".
   * @return string
   *   JSON string with data
   */
  public function getActivityItemPriceList($activity_id, $currency = "EUR") {
    $currency = strtoupper($currency);
    $path = "/activity.json/{$activity_id}/price-list?currency={$currency}";

    return $this->apiQuery($path);
  }

  /**
   * Get a list activities matching the IDs provided
   *
   * @param int|string|array $activity_ids
   *   Integer value, comma separated string or an array of integers with the
   *   activity ids for Bókun.
   * @param string $currency
   *   The requested currency value. The value should be uppercase,
   *   but the function will uppercase the string, just "in case".
   * @param string $lang
   *   The language the content should be served in
   * @return string
   *   JSON string with data
   */
  public function getActivityItemListById($activity_ids, $currency = "EUR",
                                          $lang = "EN") {
    $currency = strtoupper($currency);
    if(is_array($activity_ids)) {
      $activity_ids = implode(",",$activity_ids);
    }

    $path = "/activity.json/list-by-id?ids={$activity_ids}&currency={$currency}
    &lang={$lang}";

    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR BOOKINGS ***********************
   */

  /**
   * Get a list of reserved bookings for the guest (if any exist)
   *
   * @param $session_id
   *   The ID of the session
   * @param string $currency
   *   The requested Currency value. The value should be uppercase,
   *   but the function will uppercase the string, just "in case".
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required.
   */
  public function getReservedBookingsForGuest($session_id, $currency = "EUR") {
    $currency = strtoupper($currency);
    $path = "/booking.json/guest/{$session_id}/reserved?currency={$currency}";

    return $this->apiQuery($path);
  }

  /**
   * Move a reserved booking back to a session cart.
   *
   * @param $session_id
   *   The ID of the session
   * @param $confirmation_code
   *   The confirmation code of the booking
   * @param string $currency
   *   The requested Currency value. The value should be uppercase,
   *   but the function will uppercase the string, just "in case".
   * @param string $lang
   *   The language the content should be served in
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required.
   */
  public function moveReservedBookingBackToCart($session_id, $confirmation_code,
                                                $currency = "EUR", $lang = "EN" ) {
    $currency = strtoupper($currency);
    $path = "/booking.json/{$confirmation_code}/move-back-to-cart/session/
    {$session_id}?currency={$currency}&lang={$lang}";

    return $this->apiQuery($path);
  }

  /**
   * Abort a reserved booking.
   *
   * @param $confirmation_code
   *   The confirmation code of the booking
   * @param false $timeout
   *  Specifies whether the booking is aborted due to a timeout
   * @return string
   *  Json string with data
   *
   * TODO: Testing is required.
   */
  public function abortReservedBooking($confirmation_code, $timeout = FALSE) {
    $path = "/booking.json/{$confirmation_code}/abort-reserved?timeout={$timeout}";

    return $this->apiQuery($path);
  }

  /**
   * Fetch a booking by confirmation code.
   *
   * @param $confirmation_code
   *   The confirmation code of the booking
   * @param string $currency
   *   The requested Currency value. The value should be uppercase,
   *   but the function will uppercase the string, just "in case".
   * @param string $lang
   *   The language the content should be served in
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required.
   */
  public function getBookingItemByConfirmationCode($confirmation_code,
                                                   $currency = "ISK", $lang = "EN") {
    $currency = strtoupper($currency);
    $path = "/booking.json/booking/{$confirmation_code}?currency={$currency}&lang={$lang}";

    return $this->apiQuery($path);

  }

  /**
   * Fetch a booking by id
   *
   * @param $booking_id
   *   The id of the booking
   * @param string $currency
   *   The requested Currency value. The value should be uppercase,
   *   but the function will uppercase the string, just "in case".
   * @param string $lang
   *   The language the content should be served in
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required.
   */
  public function getBookingItemById($booking_id, $currency = "ISK", $lang = "EN" ) {
    $currency = strtoupper($currency);
    $path = "/booking.json/booking/{$booking_id}?currency={$currency}&lang={$lang}";

    return $this->apiQuery($path);
  }

  /**
   * Get summary for given bookings.
   *
   * @param $booking_id
   *  The id of the booking
   * @return string
   *  JSON string with data
   * TODO: Testing is required
   */
  public function getBookingItemSummary($booking_id) {
    $path = "booking.json/{$booking_id}/summary";
    return $this->apiQuery($path);
  }

  /**
   * Show activity booking information
   *
   * @param $id
   *   The id or confirmation code for the product booking
   * @return string
   *   JSON string with data
   * TODO: Testing is required.
   */
  public function getActivityBookingItem ($id) {
    $path = "booking.json/activity-booking/{$id}";

    return $this->apiQuery($path);
  }

  /**
   * Change the arrived status of an activity booking
   *
   * @param $confirmation_code
   *   The confirmation code for the product booking
   * @param $status
   *   The arrived status(yes/no/true/false)
   * @return string
   *   JSON string with data.
   * TODO: Testing is required.
   */
  public function changeActivityBookingItemCustomerStatus($confirmation_code, $status) {
    $status = strtolower($status);
    $path = "/booking.json/activity-booking/{$confirmation_code}/customer-status/{$status}";

    return $this->apiQuery($path);
  }

  /**
   * Get ticket for given activity booking
   *
   * @param $confirmation_code
   *   The confirmation code of the activity booking
   * @return string
   *   JSON string with data
   * TODO: Testing is required
   */
  public function getTicketForActivityBookingItem($confirmation_code) {
    $path = "/booking.json/activity-booking/{$confirmation_code}/ticket";

    return $this->apiQuery($path);
  }

  /**
   * Show route booking information
   *
   * @param $id
   *   The id or confirmation code for the product booking
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required
   */
  public function getRouteBookingItem($id) {
    $path = "/booking.json/route-booking/{$id}";

    return $this->apiQuery($path);
  }

  /**
   * Change the arrived status of a route booking
   *
   * @param $confirmation_code
   *   The confirmation code for the product booking
   * @param $status
   *   The arrived status(yes/no/true/false)
   * @return string
   *   JSON string with data.
   *
   * TODO: Testing is required.
   */
  public function changeRouteBookingItemCustomerStatus($confirmation_code, $status) {
    $status = strtolower($status);
    $path = "/booking.json/route-booking/{$confirmation_code}/customer-status/{$status}";

    return $this->apiQuery($path);
  }

  /**
   * Show car rental booking information
   *
   * @param $id
   *   The id or confirmation code for the product booking
   * @return string
   *   JSON string with data
   *
   * TODO: Testing required.
   */
  public function getCarRentalBookingItem($id) {
    $path = "/booking.json/car-rental-booking/{$id}";
    return $this->apiQuery($path);
  }

  /**
   * Change the arrived status of a car rental booking
   *
   * @param $confirmation_code
   *   The confirmation code for the product booking
   * @param $status
   *   The arrived status(yes/no/true/false)
   * @return string
   *   JSON string with data.
   *
   * TODO: Testing is required.
   */
  public function changeCarRentalBookingItemCustomerStatus($confirmation_code, $status) {
    $status = strtolower($status);
    $path = "/booking.json/car-rental-booking/{$confirmation_code}/customer-status/{$status}";

    return $this->apiQuery($path);
  }

  /**
   * Get ticket for given car rental booking
   *
   * @param $confirmation_code
   *   The confirmation code of the car rental booking
   * @return string
   *   JSON string with data.
   *
   * TODO: Testing is required.
   */
  public function getTicketForCarRentalBookingItem($confirmation_code) {
    $path = "/booking.json/car-rental-booking/{$confirmation_code}/ticket";
    return $this->apiQuery($path);
  }

  /**
   * Get ticket for given transport booking
   *
   * @param $confirmation_code
   *   The confirmation code of the transport booking
   * @return string
   *   JSON string with data.
   *
   * TODO: Testing is required.
   */
  public function getTicketTransportRentalBookingItem($confirmation_code) {

    $path = "/booking.json/transport-booking/{$confirmation_code}/ticket";
    return $this->apiQuery($path);
  }

  /**
   * Show accommodation booking information
   *
   * @param $id
   *   The id or confirmation code for the product booking
   *
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required.
   */
  public function getAccommodationBookingItem($id) {
    $path = "booking.json/accommodation-booking/{$id}";
    return $this->apiQuery($path);
  }

  /**
   * Change the arrived status of an accommodation booking
   *
   * @param $confirmation_code
   *   The confirmation code for the product booking
   * @param $status
   *   The arrived status(yes/no/true/false)
   * @return string
   *   JSON string with data.
   *
   * TODO: Testing required.
   */
  public function changeAccommodationBookingItemCustomerStatus($confirmation_code,
                                                               $status) {
    $status = strtolower($status);
    $path = "/booking.json/accommodation-booking/{$confirmation_code}
    /customer-status/{$status}";

    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR ACCOMMODATION ***********************
   */

  /**
   * Get accommodation by id
   *
   * @param $accommodation_id
   *   ID of accommodation to be fetched
   * @param string $lang
   *   The language the content should be served in
   *
   * @return string
   *   JSON string with data
   * TODO: Testing is required
   */
  public function getAccommodationItem($accommodation_id, $lang = "EN") {
    $path = "/accommodation.json/{$accommodation_id}?lang={$lang}";

    return $this->apiQuery($path);
  }

  /**
   * Get a list of all rooms for a particular Accommodation
   *
   * @param $accommodation_id
   *    ID of the accommodation
   * @return string
   *   JSON string with data
   * TODO: Testing is required
   */
  public function getListOfRoomsForAccommodationItem($accommodation_id) {
    $path = "/accommodation.json/{$accommodation_id}/rooms";
    return $this->apiQuery($path);
  }

  /**
   * Get room availabilities for one or more accommodations over an interval
   *
   * @param string|integer|array $accommodation_ids
   *   Comma seperated IDs of the accommodations
   * @param $start_date
   *   The start date of the interval (yyyy-MM-dd)
   * @param $end_date
   *   The end date of the interval (yyyy-MM-dd)
   * @param string $lang
   *   The language content should be served in
   * @param string $currency
   *   The currency code to use for calculating prices
   *
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required
   */
  public function getAvailabilitiesForAccommodations($accommodation_ids,
                                                     $start_date, $end_date,
                                                     $lang = 'EN',
                                                     $currency = "EUR") {
    $currency = strtoupper($currency);

    if (is_array($accommodation_ids)) {
      $accommodation_ids = implode(',', $accommodation_ids);
    }

    $path = "/accommodation.json/availabilities?ids={$accommodation_ids}
    &start={$start_date}&end={$end_date}&lang={$lang}&currency={$currency}";

    return $this->apiQuery($path);
  }

  /**
   * Look up accommodation by slug
   *
   * @param string $slug
   *   The slug of the accommodation to be fetched
   * @param string $lang
   *   The language the content should be served in
   *
   * @return string
   *   JSON string with data
   */
  public function getAccommodationBySlug($slug, $lang = 'EN') {
    $path = "/accommodation.json/slug/{$slug}?lang={$lang}";

    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR CAR RENTAL ***********************
   */

  /**
   * List all car rentals
   *
   * @param string $currency
   *   The currency code to use for calculating prices
   * @param string $lang
   *   The language the content should be served in
   * @return bool|string
   *   JSON string with data
   * TODO: Testing is required
   */
  public function getAllCarRentals($currency = "EUR", $lang = "EN") {
    $currency = strtoupper($currency);
    $path = "/car-rental.json/list?currency={$currency}&lang={$lang}";

    return $this->apiQuery($path);
  }

  /**
   * Look up accommodation by slug
   *
   * @param string $slug
   *   The slug of the accommodation to be fetched
   * @param string $lang
   *   The language the content should be served in
   *
   * @return string
   *   JSON string with data
   */
  public function getCarRentalBySlug($slug, $currency = "EUR", $lang = 'EN') {
    $currency = strtoupper($currency);
    $path = "/car-rental.json/slug/{$slug}?currency={$currency}&lang={$lang}";

    return $this->apiQuery($path);
  }

  /**
   * Get car rental by id
   *
   * @param string $id
   *   The ID of car rentals to be fetched
   * @param string $currency
   *   The currency code to use for calculating prices.
   * @param $language
   *   The language of the data, we would like to fetch.
   * @return string
   *   JSON string with data
   */
  public function getCarRentalItem($id, $currency = "EUR", $language = "EN") {
    $currency = strtoupper($currency);
    $path = "/car-rental.json/{$id}?currency={$currency}&lang={$language}";
    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR CATEGORY ***********************
   */

  /**
   * Get all categories for the specified product type
   *
   * @param $product_type
   *    The product type to be categorized
   * @param null $item
   *   Specifies whether to get only categories for bookable items in this
   *   product type or not
   * @param null $flags
   *   Comma separated list of required flags
   * @param string $lang
   *   The language the content should be served in
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required.
   */

  public function getAllCategories($product_type, $lang = "EN", $item = NULL,
                                   $flags = NULL ) {
    $path = "/category.json/($product_type}?lang={$lang}";

    if (is_array($flags)) {
      $flags = implode(",",$flags);
    }

    if (($item) && !($flags)) {
      $path = $path . "&item={$item}";
    }
    elseif (!($item) && ($flags)) {
      $path = $path . "&flags={$flags}";
    }
    elseif (($item) && ($flags)) {
      $path = $path . "&item={$item}" . "&flags={$flags}";
    }
    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR CHECKOUT ***********************
   */

  /**
   * Get checkout options for the shopping cart specified
   * @param $session_id
   *   The session ID of shopping cart
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required
   */
  public function getCheckoutOptions($session_id) {
    $path = "/checkout.json/options/shopping-cart/{$session_id}";
    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR COUNTRY ***********************
   */

  /**
   * Get all supported countries/nationalities
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required
   */
  public function getAllCountries() {
    $path = "/country.json/findAll";
    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR CURRENCY ***********************
   */

  /**
   * Get all supported currencies
   * @return string
   *   JSON string with data
   */
  public function getAllCurrencies() {
    $path = "/currency.json/findAll";
    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR LANGUAGE ***********************
   */

  /**
   * Get all languages supported by the channel
   * @return string
   *   JSON string with data
   */
  public function getAllLanguages() {
    $path = "/language.json/findAll";
    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR PRODUCT-LIST ***********************
   */

  /**
   * Get product list by slug
   *
   * @param $slug
   *   The slug of the Product List to be fetched
   * @param string $currency
   *   The currency code to use for calculating prices
   * @param null $flags
   *   Comma separated list of required flags
   * @param string $lang
   *   The language the content should be served in
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required
   */
  public function getProductListBySlug($slug, $currency = "EUR", $flags = NULL,
                                       $lang = "EN") {
    $currency = strtoupper($currency);

    if($flags) {
      $path = "/product-list.json/slug/{$slug}?currency={$currency}&flags={$flags}
      &lang={$lang}";
    }
    else {
      $path = "/product-list.json/slug/{$slug}?currency={$currency}&lang={$lang}";
    }
    return $this->apiQuery($path);
  }

  /**
   * Get all available product lists
   *
   * @param string $lang
   *   The language the content should be served in
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required
   */
  public function getAllProductLists($lang = "ENG") {
    $path = "/product-list.json/list?lang={$lang}";
    return $this->apiQuery($path);
  }

  /**
   * Get product list by id
   *
   * @param $product_list_id
   *   ID of the product list to be fetched
   * @param string $currency
   *   The currency code to use for calculating prices
   * @param null $flags
   *   Comma separated list of required flags
   * @param string $lang
   *   The language the content should be served in
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required
   */
  public function getProductListById($product_list_id, $currency = "EUR", $flags = NULL,
                                       $lang = "EN") {
    $currency = strtoupper($currency);

    if($flags) {
      $path = "/product-list.json/slug/{$product_list_id}?currency={$currency}&flags={$flags}&lang={$lang}";
    }
    else {
      $path = "/product-list.json/slug/{$product_list_id}?currency={$currency}&lang={$lang}";
    }
    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR QUESTION ***********************
   */

  /**
   * Get questions for the shopping cart specified
   *
   * @param $session_id
   *   The session id of shopping cart
   * @return string
   *   JSON string with data
   *
   */
  public function getQuestionForShoppingCart($session_id) {
    $path = "/question.json/shopping-cart/($session_id)";
    return $this->apiQuery($path);
  }

  /**
   * Get questions for the booking specified
   *
   * @param $booking_id
   *   The ID of the booking
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required
   */
  public function getQuestionForBookingItem($booking_id) {
    $path = "/question.json/booking/{$booking_id}";
    return $this->apiQuery($path);
  }

  /**
   * Get questions for the activity booking specified
   *
   * @param $activity_booking_id
   *   The ID of the activity booking
   * @return string
   *   JSON string with data
   * TODO: Testing is required
   */
  public function getQuestionForActivityBookingItem($activity_booking_id) {
    $path = "/question.json/activity-booking/{$activity_booking_id}";
    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR ROUTE ***********************
   */

  /**
   * List
   * @param string $currency
   *   The currency code to use for calculating prices
   * @param string $lang
   *   The language the content should be served in
   *
   * @return string
   *   JSON string with data
   * TODO: Testing is required
   */
  public function getListOfRoutes($currency="EUR", $lang="EN") {
    $path = "route.json/list?currency={$currency}&lang={$lang}";
    return $this->apiQuery($path);
  }

  /**
   * Get route by id
   *
   * @param $transport_id
   *   ID of transport route to be fetched
   * @param string $currency
   *   The currency code to use for calculating prices
   * @param string $lang
   *   The language the content should be served in
   *
   * @return string
   *   JSON string with data
   * TODO: Testing is required.
   */
  public function getRouteItembyId($transport_id, $currency="EUR", $lang="EN") {
    $path = "route.json/{$transport_id}?currency={$currency}&lang={$lang}";
    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR CART ***********************
   */

  /**
   * Get the contents of the shopping cart with the session ID supplied
   *
   * @param $session_id
   *   The session id of the shopping cart
   * @return string
   *   JSON string with data
   */
  public function getShoppingCartItemById($session_id) {
    $path = "/cart.json/{$session_id}";
    return $this->apiQuery($path);
  }

  /**
   * Remove a product booking from the shopping cart
   *
   * @param $session_id
   *   The session ID of shopping cart
   * @param $product_booking_confirmation_code
   *   The confirmation code of the product booking you want to remove
   * @return string
   *   JSON string with data
   */
  public function removeProductBookingFromCart($session_id, $product_booking_confirmation_code) {
    $path = "/cart.json/{$session_id}/remove/{$product_booking_confirmation_code}";
    return $this->apiQuery($path);
  }

  /**
   * Apply a promo code to the shopping cart
   *
   * @param $session_id
   *   The session ID of shopping cart
   * @param $promo_code
   *   The promo code you wish to apply to the shopping cart
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required
   */
  public function applyPromoCodeToCart($session_id, $promo_code) {
    $path = "/cart.json/{$session_id}/apply-promo-code/{$promo_code}";
    return $this->apiQuery($path);
  }

  /**
   * Remove any promo code from the shopping cart
   *
   * @param $session_id
   *   The session ID of shopping cart
   * @return string
   *   JSON string with data
   */
  public function removePromoCodeFromCart($session_id) {
    $path = "/cart.json/{$session_id}/remove-promo-code";
    return $this->apiQuery($path);
  }

  /**
   * Apply a gift card to the shopping cart
   *
   * @param $session_id
   *   The session ID of shopping cart
   * @param $gift_card_code
   *   The gift card code you wish to apply to the shopping cart
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required
   */
  public function applyGiftCardToCart($session_id, $gift_card_code) {
    $path = "/cart.json/{$session_id}/apply-gift-card/{$gift_card_code}";
    return $this->apiQuery($path);
  }

  /**
   * Apply an affiliate tracking code card to the shopping cart
   *
   * @param $session_id
   *   The session ID of shopping cart
   * @param $affiliate_tracking_code
   *   The affiliate tracking code you wish to apply to the shopping cart
   * @return string
   *   JSON string with data
   *
   * TODO: Testing is required
   */
  public function applyAffiliateTrackingCodeToCart($session_id, $affiliate_tracking_code) {
    $path = "/cart.json/{$session_id}/track-affiliate/{$affiliate_tracking_code}";
    return $this->apiQuery($path);
  }

  /**
   *********************** OPERATIONS FOR TAG ***********************
   */

  public function getTagGroups($lang = "EN", $flags = NULL) {
    if (is_array($flags)) {
      $flags = implode(',',$flags);
    }

    $path = "/tag.json/groups?lang={$lang}";

    if ($flags) {
      $path = $path . "&{$flags}";
    }
    return $this->apiQuery($path);
  }

  /**
   * Queries the API with a GET method.
   *
   * @param string $path
   *   The path to the API endpoint.
   * @return bool|string
   */
  private function apiQuery($path) {
    $config = $this->configFactory->get('bokun_api.configuration');
    $date = date('Y-m-d H:i:s');
    $url = $config->get('api_url');
    $signature = base64_encode(
      hash_hmac(
      "sha1", $date . $config->get('api_key') . "GET" . $path, $config->get('api_secret'), TRUE
      )
    );

    $header = [
      "X-Bokun-Date: " . $date,
      "X-Bokun-AccessKey: " . $config->get('api_key'),
      "X-Bokun-Signature: " . $signature
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url . $path);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
  }

  private function postQuery($path, $data) {
    $config = $this->configFactory->get('bokun_api.configuration');
    $date = date('Y-m-d H:i:s');
    $url = $config->get('api_url');
    $signature = base64_encode(
      hash_hmac(
        "sha1", $date . $config->get('api_key') . "GET" . $path, $config->get('api_secret'), TRUE
      )
    );

    $header = [
      "X-Bokun-Date: " . $date,
      "X-Bokun-AccessKey: " . $config->get('api_key'),
      "X-Bokun-Signature: " . $signature,
      "Content-Type: application/json; charset=UTF-8"
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url . $path);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_ENCODING, "UTF-8");

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
  }
}
